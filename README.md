# brave

> :red_exclamation_mark: This project is not maintained anymore, as I stopped using it.

Container to run Brave Browser with x11docker based on x11docker/lxde:latest.

The container includes gopass and gpg for password management.

## Run

The container can be run with support for printer, sound and clipboard using the following command:
```
x11docker --printer=socket --pulseaudio=tcp --clipboard --gpu --network=host --name brave --hostdbus -- -v "${HOME}/.config/brave/:/fakehome/brave_user/" -v "${HOME}/.config/brave/:/home/brave_user/" -- brave brave-browser --no-sandbox
```

With this command all configuration can data is stored within `.config/brave` within your home.
It requires the running user to have UID 1001.
