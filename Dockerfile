FROM		x11docker/lxde:latest
ENV		DEBIAN_FRONTEND noninteractive
ENV		XDG_RUNTIME_DIR /run/user/1001
SHELL		["/bin/bash", "-o", "pipefail", "-o", "errexit", "-c"]
RUN		set -x \
		&& useradd \
			-m \
			-s /bin/bash \
			-G sudo \
			-u 1001 \
			brave_user \
		&& apt-get update --allow-releaseinfo-change \
		&& apt-get -y install --no-install-recommends \
			ca-certificates \
			curl \
			git \
			gnupg \
			gpg-agent \
			libssl1.1 \
			libxv1 \
			mesa-utils \
			mesa-utils-extra \
			pinentry-qt \
			pinentry-tty \
			pulseaudio \
			sudo \
			systemd-sysv \
			va-driver-all \
		&& mkdir -p /tmp/bla/ \
		#&& curl -sSL "$(curl -sSL "https://api.github.com/repos/gopasspw/gopass/releases" | grep browser_download_url | grep 'linux-amd64.tar.gz' | head -1 | cut -d'"' -f4)" | gunzip | tar -xf -C /tmp/bla/ - \
		&& curl -sSL https://github.com/gopasspw/gopass/releases/download/v1.9.2/gopass-1.9.2-linux-amd64.tar.gz | gunzip | tar -C /tmp/bla/ -xf - \
		&& mv /tmp/bla/gopass /usr/local/bin/ \
		&& rm -rf /tmp/bla \
		&& curl -L -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add - \
		&& echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" > /etc/apt/sources.list.d/brave-browser-release.list \
		&& apt-get update \
		&& ( \
			apt-get -y install --no-install-recommends vim brave-keyring brave-browser \
			|| apt-get install --no-install-recommends --fix-broken -y \
		) \
		&& apt-get autoclean

VOLUME		/fakehome/brave_user/
VOLUME		/home/brave_user/
